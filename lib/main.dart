import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

void main() {
  runApp(MyApp());
}

/////////View start/////////////////
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Study',
        home: BlocProvider(
          create: (_) => Interactor(),
          child: CounterPage(),
        ));
  }
}

class CounterPage extends StatelessWidget {

  Presenter _presenter = Presenter();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: BlocBuilder<Interactor, AppState>(
        builder: (_, appState) {
          print(appState is LoadingData);
          if (appState is LoadedData) {
            return Center(
              child: Text(appState.getData.toString()),
            );
          } else if (appState is LoadingData) {
            return Center(child: CircularProgressIndicator(),);
          } else {
            return Center(
              child: Text('error'),
            );
          }
        },
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () {
          _presenter.btnPressed(context.bloc);
        },
      ),
    );
  }
}

class Presenter{
  void btnPressed(_){
    _.add(PressedBTN());
  }
}

////Use Cases ///////////
class Interactor extends Bloc<Event, AppState> {
  Repository repo;

  Interactor() : super(LoadedData(0)) {
    repo = Repository();
  }

  @override
  Stream<AppState> mapEventToState(Event event) async* {
    if (event is PressedBTN) {
      yield LoadingData();
      int res = await repo.loadingData();
      yield LoadedData(res);
    }
  }
}

class Repository {
  Data data;

  Repository() {
    count = 0;
    data = Data();
  }

  int count;

  Future<int> loadingData() async {
    count += await data.getIncrement();
    return count;
  }
}

class Data {
  Future<int> getIncrement() async {
    await Future.delayed(Duration(milliseconds: 500));
    return 1;
  }
}

abstract class AppState {
  int getCount;
}

class LoadedData extends AppState {
  int _count;

  LoadedData(this._count);

  int get getData => _count;
}

class LoadingData extends AppState {}

abstract class Event {
//  const Event();
}

class PressedBTN extends Event {}